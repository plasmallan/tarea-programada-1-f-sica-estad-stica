# Simulación de gota de crema en café.

# Imports

import numpy as np
import random as rd
import matplotlib.pyplot as plt

# Cantidad de partículas
N = 100

#Arreglo de matriz

# Plano en donde yacen las partículas, el café
M = np.zeros((N,N))

# Se asigna valor de 1 a las posiciones en las que hay partícula.
# Se define el estado inicial de 100 partículas en un cuadrado de 10x10.

for i in range(0,N):
    if 45<= i <= 54:
        for j in range(0,N):
            if 45<= j <= 54:
                M[i][j] = 1
# Primer cálculo de entropía, debe ser 0.
splits = np.split(M,N) # Se subdivide el espacio M en N subarreglos

ent = 0 # Sumador de entropía

plt.matshow(M)
plt.xlabel("x")
plt.ylabel("y")
plt.title("Posición de las partículas de crema en el café \n Estado Inicial")
plt.show()

# Se comienza a iterar sobre todas las partículas.
# Si en un campo hay un estado en 1, se chequean las posiciones alrededor
# Se escoge una posición desocupada de forma aleatoria.

p = 0 # Contaador de pasos
PP = [N/100,N/10,N,N*10,N*N]
P = N*N # Paso máximo

steps = []
entropy = []
prob = []
steps.append(p)
entropy.append(ent)

percell = np.zeros(N)

while p<=P:
    for k in range(0,N):
        for l in range(0,N):
            if M.sum() != N:
                print(k)
                print(l)
                print(p)
                break
            
            direc = []
            
            if M[k][l] == 1: # Se selecciona celda ocupada

# Cada partícula en cada paso tiene una probabilidad de 0.25 de moverse en una dirección.
# Si está bloqueada en algún punto, esto se suma a una probabilidad de quedarse quieta.

                if k != N-1 and l != N-1 and k != 0 and l != 0: # Partículas en el bulk
                    if M[k - 1][l] != 1:
                        direc.append("UP")
                    else:
                        direc.append(0)
                    
                    if M[k][l+1] != 1:
                        direc.append("RIGHT")
                    else:
                        direc.append(0)
                        
                    if M[k+1][l] != 1:
                        direc.append("DOWN")
                    else:
                        direc.append(0)
                        
                    if M[k][l-1] != 1:
                        direc.append("LEFT")
                    else:
                        direc.append(0)

                elif k == N-1 and l != N-1: # Partículas en la parte más baja
                    direc.append(0)

                    if M[k-1][l] != 1:
                        direc.append("UP")
                    else:
                        direc.append(0)
                    
                    if M[k][l+1] != 1:
                        direc.append("RIGHT")
                    else:
                        direc.append(0)
                        
                    if M[k][l-1] != 1:
                        direc.append("LEFT")
                    else:
                        direc.append(0)

                elif l == N-1 and k != N-1: # Partículas en lado derecho
                    direc.append(0)

                    if M[k-1][l] != 1:
                        direc.append("UP")
                    else:
                        direc.append(0)
                    
                    if M[k+1][l] != 1:
                        direc.append("DOWN")
                    else:
                        direc.append(0)
                        
                    if M[k][l-1] != 1:
                        direc.append("LEFT")
                    else:
                        direc.append(0)

                elif l == N-1 and k == N-1: # Partículas en la esquina inferior derecha
                    direc.append(0)

                    direc.append(0)

                    if M[k-1][l] != 1:
                        direc.append("UP")
                    else:
                        direc.append(0)
                    
                    if M[k][l-1] != 1:
                        direc.append("LEFT")
                    else:
                        direc.append(0)

                elif k == 0 and l != 0: # Partículas en el lado superior
                    direc.append(0)
                    
                    if M[k][l+1] != 1:
                        direc.append("RIGHT")
                    else:
                        direc.append(0)
                        
                    if M[k+1][l] != 1:
                        direc.append("DOWN")
                    else:
                        direc.append(0)
                        
                    if M[k][l-1] != 1:
                        direc.append("LEFT")
                    else:
                        direc.append(0)

                elif k != 0 and l == 0: # Partículas en el lado izquierdo
                    direc.append(0)
                    
                    if M[k-1][l] != 1:
                        direc.append("UP")
                    else:
                        direc.append(0)
                    
                    if M[k][l+1] != 1:
                        direc.append("RIGHT")
                    else:
                        direc.append(0)
                        
                    if M[k+1][l] != 1:
                        direc.append("DOWN")
                    else:
                        direc.append(0)

                elif k == 0 and l == 0: # Partículas en la esquina superior izquierda
                    direc.append(0)

                    direc.append(0)
                    
                    if M[k][l+1] != 1:
                        direc.append("RIGHT")
                    else:
                        direc.append(0)
                        
                    if M[k+1][l] != 1:
                        direc.append("DOWN")
                    else:
                        direc.append(0)

                elif k == 0 and l == N-1: # Partículas en la esquina superior derecha
                    direc.append(0)

                    direc.append(0)
                        
                    if M[k+1][l] != 1:
                        direc.append("DOWN")
                    else:
                        direc.append(0)
                        
                    if M[k][l-1] != 1:
                        direc.append("LEFT")
                    else:
                        direc.append(0)

                elif k == N-1 and l == 0: # Partículas en la esquina inferior izquierda
                    direc.append(0)

                    direc.append(0)
                    
                    if M[k-1][l] != 1:
                        direc.append("UP")
                    else:
                        direc.append(0)
                    
                    if M[k][l+1] != 1:
                        direc.append("RIGHT")
                    else:
                        direc.append(0)
                        
# Se toma un camino de forma aleatoria de los restantes posibles.
                step = rd.choice(direc)
                    
                if step == "UP":
                    M[k][l] = 0
                    M[k-1][l] = 1

                elif step == "RIGHT":
                    M[k][l] = 0
                    M[k][l+1] = 1

                elif step == "DOWN":
                    M[k][l] = 0
                    M[k+1][l] = 1

                elif step == "LEFT":
                    M[k][l] = 0
                    M[k][l-1] = 1

                else:
                    pass
            
    if M.sum() != N:
        print(k)
        print(l)
        print(p)
        break

    ent = 0 # Sumador de entropía
    p = p + 1
    
# Parte 2: Se toma cada estado en cada paso y se suma la cantidad de partículas
# Por paso se acumulan la cantidad de partículas en ese estado
    for n in range(N):
        percell[n] = percell[n] + np.sum(M[n])

# Se define la probabilidad por paso como el acumulado de partículas por estado
# Entre el paso por el número de partículas, de tal forma que se crea un acumulado
# de probabilidad que por paso siempre sumará 1.
    ps = percell/(p*N)
    prob.append(ps)
    
# Se calcula la entropía siguiendo la fórmula de Gibbs, se accesa la probabilidad.
# Almacenada acumulada por estado para calcularla.
    for pp in range(N):
        ent = ent - np.nansum(ps[pp]*np.log(ps[pp]))

# Se almacena el paso y la entropía del sistema en este paso.
    steps.append(p)
    entropy.append(ent)

    if p in PP:
        
        print("=======================================================================")
        print("Gráficas de Estado Final, Entropía y Distribución de Probabilidad para " + str(int(p)) + " pasos")
# Se grafica el estado final.  
        plt.matshow(M)
        plt.xlabel("x")
        plt.ylabel("y")
        plt.title("Posición de las partículas de crema en el café \n Estado Final para " + str(int(p)) + " pasos")
        plt.show()

# Se grafica la entropía como función de los pasos.
        plt.plot(steps,entropy)
        plt.xlabel("Paso")
        plt.ylabel("Entropía")
        plt.title("Evolución de la entropía en el tiempo para " + str(int(p)) + " pasos")
        plt.show()

# Se grafica la distribución de la probabilidad por estado para el estado final.
        plt.plot(range(N),ps)
        plt.xlabel("Estado")
        plt.ylabel("Probabilidad para " + str(int(p)) + " pasos")
        plt.title("Distribución de la probabilidad en los estados para " + str(int(p)) + " pasos")
        plt.show()
        print("=======================================================================")
