import pygame
import time
import numpy as np
import random as rd
import matplotlib.pyplot as plt
import pylab as pyl
import copy
from copy import deepcopy


width, height = 800, 800
nxC, nyC = 80, 80
dimCW = width / nxC
dimCH = height / nyC

pygame.init()

screen = pygame.display.set_mode([height, width])

bg = 10, 10, 10
screen.fill(bg)

while True:
    pass

# Execution Loop
while True:
    screen.fill(bg)
    time.sleep(0.1)
    
    for y in range(0,nyC):
        for x in range(0,nxC):
            poly = [((x) * dimCW, y * dimCH),
                    ((x + 1) * dimCW, y * dimCH),
                    ((x + 1) * dimCW, (y + 1) * dimCH),
                    ((x) * dimCW, (y + 1) * dimCH)]

            pygame.draw.polygon(screen, (128, 128, 128), poly, 1)
            
    pygame.display.flip()
